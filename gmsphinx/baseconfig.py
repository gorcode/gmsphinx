from os import path

cur_dir = path.abspath(path.dirname(__file__))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.todo',
    'sphinx.ext.imgmath',
    'sphinx.ext.ifconfig',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.extlinks',
    'sphinxcontrib.httpdomain',
    'sphinxcontrib.mermaid',
    'gmsphinx.gmdomain',
]

templates_path = [cur_dir + '/templates']

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store',
                    'venv', '**.inc.rst']

master_doc = 'index'
source_suffix = '.rst'

pygments_style = 'sphinx'

nitpicky = True

extlinks = {'jira': ('https://work.greyorange.com/jira/browse/%s', '')}

html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'prev_next_buttons_location': 'both',
    'logo_only': True,
    'style_external_links': True,
}

html_static_path = [cur_dir + '/static']

html_context = {
    'css_files': [
        cur_dir + '/static/theme_overrides.css',  # override wide tables in RTD theme
    ],
}

html_show_sphinx = True

html_logo = cur_dir + "/static/logo.png"

todo_include_todos = True
