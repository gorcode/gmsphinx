# -*- coding: utf-8 -*-

import io
from setuptools import setup, find_packages

version = '0.2.0'

setup(
    name='gmsphinx',
    version=version,
    description='Sphinx customizations for greymatter',
    platforms='any',
    packages=find_packages(),
    package_data={'gmsphinx': [
        'static/*.*',
        'templates/*.*',
    ]},
    include_package_data=True,
    # Using exact version of requires because we don't want
    # this to be specified in each repo
    install_requires=[
        'sphinx==5.0.2',
        'sphinx_rtd_theme==1.0.0',
        'sphinx-autobuild==2021.3.14',
        'sphinxcontrib-httpdomain==1.8.0',
        'sphinx-paramlinks==0.5.4',
        'sphinxcontrib-mermaid==0.7.1',
    ],
    namespace_packages=['gmsphinx'],
)
